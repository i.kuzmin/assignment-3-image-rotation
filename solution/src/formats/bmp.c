#include <formats/bmp.h>
#include <image.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#define SIGNATURE 0x4D42
#define RESERVED 0
#define OFFSET 54
#define SIZE 40
#define PLANES 1
#define BIT_COUNT 24
#define COMPRESSION 0
#define PELS_PER_METER 0
#define CLR_USED 0
#define CLR_IMPORTANT 0

static long calc_padding(uint64_t width) {
    return (long) (4 - width * sizeof(struct pixel) % 4) % 4;
}

enum read_status validate_header(struct bmp_header header) {
    if (header.bfType != SIGNATURE)
        return READ_INVALID_SIGNATURE;

    if (header.biBitCount != 24)
        return READ_INVALID_BITS;

    return READ_OK;
}

enum read_status from_bmp( FILE* in, struct image* img ) {
    if (in == NULL || img == NULL)
        return READ_ERROR;

    struct bmp_header header = {0};

    if (fread(&header, sizeof(struct bmp_header), 1, in) != 1)
        return READ_INVALID_HEADER;

    enum read_status status = validate_header(header);
    if (status != READ_OK) {
        return status;
    }

    fseek(in, header.bOffBits, SEEK_SET);

    *img = image_create(header.biWidth, header.biHeight);
    long padding = calc_padding(img->width);

    for (size_t i = 0; i < img->height; i++) {
        if (fread(img->data + i * img->width, sizeof(struct pixel) * img->width, 1, in) != 1)
            return READ_ERROR;

        fseek(in, padding, SEEK_CUR);
    }

    return READ_OK;
}

struct bmp_header create_bmp_header(struct image const* img) {
    uint32_t size_image = (img->width * sizeof(struct pixel)) * img->height;
    return (struct bmp_header) {
            .bfType = SIGNATURE,
            .bfReserved = RESERVED,
            .bOffBits = OFFSET,
            .biSize = SIZE,
            .biWidth = img->width,
            .biHeight = img->height,
            .biPlanes = PLANES,
            .biBitCount = BIT_COUNT,
            .biCompression = COMPRESSION,
            .biSizeImage = size_image,
            .bfileSize = size_image + OFFSET,
            .biXPelsPerMeter = PELS_PER_METER,
            .biYPelsPerMeter = PELS_PER_METER,
            .biClrUsed = CLR_USED,
            .biClrImportant = CLR_IMPORTANT
    };
}

enum write_status to_bmp( FILE* out, struct image const* img ) {
    if (out == NULL || img == NULL || img->data == NULL)
        return WRITE_ERROR;

    long padding = calc_padding(img->width);

    struct bmp_header header = create_bmp_header(img);

    fwrite(&header, sizeof(struct bmp_header), 1, out);
    for (size_t i = 0; i < img->height; i++) {
        fwrite(img->data + i * img->width, sizeof(struct pixel), img->width, out);
        fseek(out, padding, SEEK_CUR);
    }

    return WRITE_OK;
}
