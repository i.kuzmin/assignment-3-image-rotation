#include <stdlib.h>
#include <transforms/rotate.h>

struct image rotate( struct image const source ) {
    struct image rotated = image_create(source.height, source.width);

    for (size_t i = 0; i < rotated.height; i++) {
        for (size_t j = 0; j < rotated.width; j++) {
            image_set_pixel(&rotated, i, j, image_get_pixel(&source, source.height - j - 1, i));
        }
    }
    return rotated;
}
