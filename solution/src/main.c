#include <formats/bmp.h>
#include <stdio.h>
#include <transforms/rotate.h>

static const char *READ_ERRORS[] = {
    [READ_INVALID_SIGNATURE] = "File has invalid signature",
    [READ_INVALID_BITS] = "File has bit count which not supporting",
    [READ_INVALID_HEADER] = "Image header isn't valid",
    [READ_ERROR] = "Error while reading file"
};

static const char *WRITE_ERRORS[] = {
    [WRITE_ERROR] = "File has invalid signature",
};

void free_resources(FILE *in, FILE *out, struct image* image, struct image *rotated) {
    if (in != NULL)
        fclose(in);

    if (out != NULL)
        fclose(out);

    if (image != NULL)
        image_destroy(image);

    if (rotated != NULL)
        image_destroy(rotated);
}

int main( int argc, char** argv ) {
    if (argc != 3) {
        printf("Arguments number error");
        return 1;
    }

    FILE* in = fopen(argv[1], "rb");
    if (in == NULL) {
        printf("Can't open file %s for read", argv[1]);
        return 1;
    }

    FILE* out = fopen(argv[2], "wb");
    if (out == NULL) {
        printf("Can't open file %s for write", argv[2]);
        free_resources(in, NULL, NULL, NULL);
        return 1;
    }

    struct image image;
    enum read_status read_status = from_bmp(in, &image);

    if (read_status != READ_OK) {
        printf("%s\n", READ_ERRORS[read_status]);
        free_resources(in, out, &image, NULL);
        return 2;
    }

    struct image rotated = rotate(image);
    enum write_status write_status = to_bmp(out, &rotated);

    if (write_status != WRITE_OK) {
        printf("%s", WRITE_ERRORS[write_status]);
        free_resources(in, out, &image, &rotated);
        return 3;
    }
    printf("File written");

    free_resources(in, out, &image, &rotated);

    return 0;
}
