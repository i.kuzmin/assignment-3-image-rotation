#include <image.h>
#include <stdlib.h>

struct image image_create(uint64_t width, uint64_t height) {
    struct pixel* data = malloc(sizeof(struct pixel) * width * height);

    if (data == NULL) {
        return (struct image) {0};
    }

    return (struct image) { .width = width, .height = height, .data = data };
}

void image_destroy(struct image* image) {
    if (image->data != NULL)
        free(image->data);
}

struct pixel image_get_pixel(struct image const* image, uint64_t x, uint64_t y) {
    return image->data[x * image->width + y];
}

void image_set_pixel(struct image const* image, uint64_t x, uint64_t y, struct pixel pixel) {
    image->data[x * image->width + y] = pixel;
}
