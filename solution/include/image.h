#include <stdint.h>
#pragma once

struct pixel { uint8_t b, g, r; };

struct image {
  uint64_t width, height;
  struct pixel* data;
};

struct image image_create(uint64_t width, uint64_t height);
void image_destroy(struct image* image);

struct pixel image_get_pixel(struct image const* image, uint64_t x, uint64_t y);
void image_set_pixel(struct image const* image, uint64_t x, uint64_t y, struct pixel pixel);

enum read_status  {
  READ_OK = 0,
  READ_INVALID_SIGNATURE,
  READ_INVALID_BITS,
  READ_INVALID_HEADER,
  READ_ERROR
  /* коды других ошибок  */
};

enum  write_status  {
  WRITE_OK = 0,
  WRITE_ERROR
  /* коды других ошибок  */
};
